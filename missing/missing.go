package main

import (
	"encoding/json"
	"fmt"
)

func main() {
	data := []byte(`
	{
		"user": "saitama",
		"action": "punch"
	}
	`)

	// Rule of thumb: Have seperate types for marshaling (than business logic)
	// DB <-> APP <-> API
	// Job   Job     Job
	// Stable Fluid  Very stable

	// var j map[string]any // Option: Use map[string]any - check keys
	// Look at mapstructure

	//var j Job
	// Option: Pre-fill with defaults (Miki's favorite)
	j := Job{
		Count: 1,
	}
	if err := json.Unmarshal(data, &j); err != nil {
		fmt.Println("ERROR:", err)
		return
	}
	fmt.Printf("%#v\n", j)
}

type Job struct {
	User   string
	Action string
	// Count  *int // Option: Use pointers
	Count int
}
