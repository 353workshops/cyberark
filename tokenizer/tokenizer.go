package tokenizer

import (
	"strings"
)

var (
	// "Who's on first?" -> [who s on first]
	suffixes = []string{"ed", "ing", "s"}
)

func init() {
	// runs before main (when package is imported)
}

// working, works, worked -> work
func Stem(word string) string {
	for _, s := range suffixes {
		if strings.HasSuffix(word, s) {
			return word[:len(word)-len(s)]
		}
	}

	return word
}

func initialSplit(text string) []string {
	size := len(text) / 5 // average case has 20% tokens
	fs := make([]string, 0, size)
	i := 0
	for i < len(text) {
		// eat start
		for i < len(text) && !isLetter(text[i]) {
			i++
		}
		if i == len(text) {
			break
		}

		j := i + 1
		for j < len(text) && isLetter(text[j]) {
			j++
		}
		fs = append(fs, text[i:j])

		i = j
	}
	return fs
}

func isLetter(b byte) bool {
	return (b >= 'a' && b <= 'z') || (b >= 'A' && b <= 'Z')
}

func Tokenize(text string) []string {
	words := initialSplit(text)
	tokens := make([]string, 0, int(0.9*float64(len(words))))
	for _, tok := range words {
		tok = strings.ToLower(tok)
		tok = Stem(tok)
		if tok != "" {
			tokens = append(tokens, tok)
		}
	}
	return tokens
}
