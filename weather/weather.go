package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log/slog"
	"net/http"
	"os"
	"time"

	"net/http/pprof"
)

func main() {

	/* Why we use ctxKeyType
	type T string
	m := make(map[any]int)
	m["hi"] = 1
	m[T("hi")] = 2
	fmt.Println(m)
	return
	*/

	host, err := os.Hostname()
	if err != nil {
		host = "<unknown>"
	}
	s := Server{
		logger: slog.Default().With("host", host),
	}

	mux := http.NewServeMux() // mux implements http.Handler
	mux.HandleFunc("/health", s.healthHandler)
	// mux.HandleFunc("/record", s.recordHandler)
	h := topMiddleware(s.logger, http.HandlerFunc(s.recordHandler))
	mux.Handle("/record", h)

	if os.Getenv("PROFILE") == "yes" {
		mux.HandleFunc("/debug/pprof/profile", pprof.Profile)
	}
	// Built-in server mux (router):
	// - if path ends with / does prefix match
	// - else exact match
	// 1.22 adds methods and path parts: "POST /users/{id}"
	// otherwise: chi, gorllia mux, gin, echo, fiber, ...
	// mux will panic if you add the same path twice

	srv := http.Server{
		// auth globally
		// Handler: authMiddleware(mux),
		Handler: mux,
		Addr:    ":8080",

		ReadTimeout:       15 * time.Second,
		WriteTimeout:      15 * time.Second,
		ReadHeaderTimeout: 400 * time.Millisecond,
	}
	s.logger.Info("server starting", "address", srv.Addr, "pid", os.Getpid())
	if srv.ListenAndServe(); err != nil {
		s.logger.Error("can't serve", "error", err)
		os.Exit(1)
	}

}

// Never do this IRL!!!
var authDB = map[string]string{
	"joe": "baz00ka",
}

type ctxVars struct {
	Login string
	// TODO: Request ID, ...
}

type ctxKeyType string

var ctxKey ctxKeyType = "ctxValues"

func topMiddleware(logger *slog.Logger, handler http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		// before handler
		login, passwd, ok := r.BasicAuth()
		if !ok {
			logger.Error("missing login info", "remote", r.RemoteAddr)
			http.Error(w, "bad auth", http.StatusUnauthorized)
			return
		}

		p, ok := authDB[login]
		if !ok || p != passwd {
			logger.Error("bad login info", "remote", r.RemoteAddr, "login", login)
			http.Error(w, "bad auth", http.StatusUnauthorized)
			return
		}

		vars := ctxVars{
			Login: login,
		}
		ctx := context.WithValue(r.Context(), ctxKey, &vars)
		r = r.WithContext(ctx)

		handler.ServeHTTP(w, r)

		// after handler
		// if  you want return value from handler, pass your own http.ResponseWriter
		// vars might changed from lower functions
	}

	return http.HandlerFunc(fn)
}

func (s *Server) recordHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "bad method", http.StatusMethodNotAllowed)
		return
	}

	vars, ok := r.Context().Value(ctxKey).(*ctxVars)
	if !ok {
		s.logger.Error("bad context")
		http.Error(w, "context", http.StatusInternalServerError)
		return
	}

	s.logger.Info("new record", "login", vars.Login, "remote", r.RemoteAddr)

	// Step 1: Unmarshal + Vaidate
	var rec Record
	rdr := http.MaxBytesReader(w, r.Body, 1<<20) // Read up to 1MB
	if err := json.NewDecoder(rdr).Decode(&rec); err != nil {
		s.logger.Error("bad json", "error", err)
		// Security: don't return err.Error() to the client
		http.Error(w, "bad request", http.StatusBadRequest)
		return
	}

	if err := rec.Validate(); err != nil {
		s.logger.Error("bad record", "error", err)
		// Security: don't return err.Error() to the client
		http.Error(w, "bad request", http.StatusBadRequest)
		return
	}

	// Step 2: Work
	// slog.Info("new record", "record", record)

	// TODO: Write to database ...

	// Step 3: Reply
	reply := map[string]any{
		"time":  time.Now(),
		"error": nil,
	}
	w.Header().Set("content-type", "application/json")
	w.WriteHeader(http.StatusCreated)
	// Option 1: Use json.Encoder
	if err := json.NewEncoder(w).Encode(reply); err != nil {
		// Can't change status
		s.logger.Error("can't encode reply", "error", err)
	}
	// Option 2: First json.Marshal, check error then write
}

func (s *Server) healthHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "bad method", http.StatusMethodNotAllowed)
		return
	}

	s.logger.Info("health check", "remote", r.RemoteAddr)
	fmt.Fprintln(w, "OK") // TODO: Check db connection ...
}

type Server struct {
	logger *slog.Logger // *log.Logger
	// TODO: db connection ...
}

func (r Record) Validate() error {
	return nil // FIXME
}

type Record struct {
	StationID string `json:"station_id"`
	Time      time.Time
	Wind      float64 // MPH
	Temp      float64 // Fahrenheit
}
