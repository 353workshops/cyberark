package main

import (
	"io"
	"log/slog"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestServer_healthHandler(t *testing.T) {
	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, "http://localhost:8080/health", nil)

	s := Server{slog.Default()}
	// Problems:
	// - bypass mux
	// - bypass middleware
	// Solutions:
	// - Run a server
	// - create buildMux() function and test via mux
	s.healthHandler(w, r)

	resp := w.Result()
	require.Equal(t, http.StatusOK, resp.StatusCode)
	data, err := io.ReadAll(resp.Body)
	require.NoError(t, err)
	require.Equal(t, []byte("OK\n"), data)
}
