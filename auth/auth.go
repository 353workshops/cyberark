package main

import (
	"errors"
	"fmt"
	"log"
	"time"
)

func main() {
	if err := login("bugs", "duckseason"); err != nil {
		// ue := err.(UserError) // might panic
		// ue, ok := err.(UserError) // won't panic
		var ue *UserError
		if errors.As(err, &ue) {
			log.Printf("USER ERROR: %#v\n", ue)
		} else {
			fmt.Println("GENERIC ERROR:", err)
		}
	}
}

type StringError string

func (se StringError) Error() string {
	return string(se)
}

type UserError struct {
	Login  string
	Time   time.Time
	Reason string
}

func (ue *UserError) Error() string {
	return fmt.Sprintf("[%s] %q: %s", ue.Time.Format(time.RFC3339), ue.Login, ue.Reason)
}

func login(login, passwd string) error {
	if login == "joe" && passwd == "baz00ka" {
		return nil
	}

	err := UserError{
		Login:  login,
		Time:   time.Now(),
		Reason: "bad authentication",
	}
	return &err
}
