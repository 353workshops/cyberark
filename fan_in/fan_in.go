package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func main() {
	ch1, ch2 := producer("p1", 5), producer("p2", 3)
	for val := range fanIn(ch1, ch2) {
		fmt.Println(val)
	}
}

func fanIn(chans ...chan string) chan string {
	// chans is []chan string
	out := make(chan string)

	var wg sync.WaitGroup
	wg.Add(len(chans))
	for _, ch := range chans {
		// wg.Add(1)
		ch := ch
		go func() {
			defer wg.Done()
			for msg := range ch {
				out <- msg
			}
		}()
	}

	go func() {
		wg.Wait()
		close(out)
	}()

	return out
}

func producer(name string, size int) chan string {
	ch := make(chan string)

	go func() {
		defer close(ch)

		for i := 0; i < size; i++ {
			msg := fmt.Sprintf("%s - %d", name, i)
			ch <- msg
			randSleep()
		}
	}()

	return ch
}

func randSleep() {
	time.Sleep(time.Duration(rand.Intn(100)) * time.Millisecond)
}
