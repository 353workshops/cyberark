package main

import (
	"context"
	"fmt"
	"time"
)

func main() {
	go fmt.Println("goroutine")
	fmt.Println("main")

	for i := 0; i < 5; i++ {
		// Solution 3: Upgrade to go 1.22 (Feb 2024)
		// Solution 2: Loop level variable
		i := i // Will shadow "i" form line 12
		go func() {
			fmt.Println("gr:", i)
		}()
		/* Solution 1: Pass an argument
		go func(j int) {
			fmt.Println("gr:", j)
		}(i)
		*/
		/* BUG: All goroutines use the same "i" from line 12
		go func() {
			fmt.Println("gr:", i)
		}()
		*/
	}

	time.Sleep(10 * time.Millisecond)

	ch := make(chan string)
	go func() {
		ch <- "hi" // send
	}()
	msg := <-ch // receive
	fmt.Println(msg)

	fmt.Println("sorted:", sleepSort([]int{20, 30, 10})) // [10 20 30]

	// Producer
	go func() {
		for i := 0; i < 3; i++ {
			ch <- fmt.Sprintf("msg #%d", i)
		}
		close(ch)
	}()

	for msg := range ch {
		fmt.Println("got:", msg)
	}

	/* What range over channel does
	for {
		msg, ok := <-ch
		if !ok {
			break
		}
		fmt.Println("got:", msg)
	}
	*/

	msg = <-ch // receive from closed channel
	fmt.Printf("closed: %#v\n", msg)

	msg, ok := <-ch
	fmt.Printf("closed: %#v (ok=%v)\n", msg, ok)

	// ch <- "hi" // send to closed channel (panic)
	// Aside: Scope
	a := 1
	{
		// a := 2
		a = 2
		fmt.Println("inner:", a)
	}
	fmt.Println("outer:", a)

	answer := func() (int, error) {
		time.Sleep(time.Millisecond)
		return 42, nil
	}
	fch := Future(answer)
	val := <-fch
	fmt.Println("future:", val)
	// fmt.Println(fch)
	// ... More work

	ch1, ch2 := make(chan int, 1), make(chan int, 1)
	go func() {
		time.Sleep(10 * time.Millisecond)
		ch1 <- 1
	}()
	go func() {
		time.Sleep(20 * time.Millisecond)
		ch2 <- 2
	}()

	done := make(chan struct{})
	go func() {
		// Some kind of manual reason
		time.Sleep(time.Millisecond)
		close(done)
	}()

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Millisecond)
	defer cancel()

	select {
	case val := <-ch1:
		fmt.Println("ch1:", val)
	case val := <-ch2:
		fmt.Println("ch2:", val)
	case <-ctx.Done():
		fmt.Println("context expired:", ctx.Err())
		/*
			case <-time.After(5 * time.Millisecond):
				fmt.Println("timeout")
			case <-done:
				fmt.Println("cancel")
		*/
	}

	// select{} // block forever without consuming CPU

	ch3 := make(chan int)
	select {
	case ch3 <- 7:
		fmt.Println("send")
	case <-time.After(time.Millisecond):
		fmt.Println("send timeout")
	}
}

// Roughly context

func WithTimeout(timeout time.Duration) (*Ctx, func()) {
	ctx := Ctx{
		done: make(chan struct{}),
	}

	go func() {
		time.Sleep(timeout)
		ctx.err = fmt.Errorf("context expired")
		// FIXME: Mutex
		close(ctx.done)
	}()

	fn := func() {
		ctx.err = fmt.Errorf("context canceled")
		close(ctx.done)

	}

	return &ctx, fn
}

func (c *Ctx) Err() error {
	return c.err
}

func (c *Ctx) Done() chan struct{} {
	return c.done
}

type Ctx struct {
	done chan struct{}
	err  error
}

func Future[T any](fn func() (T, error)) <-chan Result[T] {
	// buffered channel to avoid goroutine leak
	ch := make(chan Result[T], 1)

	go func() {
		v, err := fn()
		ch <- Result[T]{v, err}
	}()

	return ch
}

type Result[T any] struct {
	Value T
	Err   error
}

/* Channel Semantics
- send/receive will block until opposite action[*]
	- buffered channel of size "n" has "n" non-blocking sends
- receive from a closed channel will return the zero value
	- use ", ok" to find out if channel is closed
- send to a closed channel will panic
	- Only the channel owner should close it
- closing a closed channel will panic
- send/receive to/from a nil channel will block forever
	- var ch chan int // ch is nil
*/

/*
	For each value "n" in values spin a goroutine that

- will sleep "n" milliseconds
- will send "n" over a channel
Collect all values from the channel to a slice and return it

Hint: time.Sleep(time.Duration(n) * time.Millisecond)
for range values {

}
out := make([]int)
*/
func sleepSort(values []int) []int {
	ch := make(chan int)
	for _, n := range values {
		n := n
		go func() {
			time.Sleep(time.Duration(n) * time.Millisecond)
			ch <- n
		}()
	}

	out := make([]int, len(values))
	for i := range values {
		out[i] = <-ch
	}
	return out
}

// Two kinds of problems:
// - Synchronization: Access to shared resource
// - Coordination: Coordinate between goroutines
// Concurrency: Undefined, out of order, execution.
