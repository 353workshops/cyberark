package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Print(Relu(-7))
	fmt.Print(Relu(0))
	fmt.Print(Relu(7.0))
	fmt.Print(Relu(time.March))
	fmt.Println()

	// aside: const in Go has type when it's being used
	const n = 1
	fmt.Println(1.1 + n)

	fmt.Println(Max([]int{3, 1, 2}))     // 3 <nil>
	fmt.Println(Max([]float64{3, 1, 2})) // 3 <nil>
	fmt.Println(Max([]float64{}))        // 0 error...
	fmt.Println(Max[int](nil))           // must help compiler, nil is untyped
}

type OrderedS interface {
	Ordered | ~string
}

// if values is empty, return error
func Max[T OrderedS](values []T) (T, error) {
	if len(values) == 0 {
		var zero T
		return zero, fmt.Errorf("Max on empty slice")
	}

	m := values[0]
	for _, v := range values[1:] {
		if v > m {
			m = v
		}
	}
	return m, nil
}

type Ordered interface {
	~int | ~float64 // | time.Month
}

// T is type constraint
// func Relu(n Ordered) Ordered { // won't compile
func Relu[T Ordered](n T) T {
	if n < 0 {
		return 0
	}

	return n
}

/*
func ReluInt(n int) int {
	if n < 0 {
		return 0
	}

	return n
}

func ReluFloat64(n float64) float64 {
	if n < 0 {
		return 0
	}

	return n
}

*/

// Go uses GC shape stenciling for type constraints:
// Two concrete types are in the same gcshape grouping if and only if they have
// the same underlying type or they are both pointer types.
