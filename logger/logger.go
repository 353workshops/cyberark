package main

import (
	"fmt"
	"io"
	"os"
)

func main() {
	// l := Logger{os.Stdout}
	l := NewLogger(os.Stdout)
	l.Info("server is up")
}

func NewLogger(w io.Writer) Logger {
	l := Logger{w: w, s: nopSyncer{}}
	if s, ok := w.(syncer); ok {
		l.s = s
	}
	return l
}

/*
type WriteSyncer interface {
	io.Writer
	Sync() error
}
*/

type syncer interface {
	Sync() error
}

type nopSyncer struct{}

func (nopSyncer) Sync() error { return nil }

func (l Logger) Info(msg string) {
	fmt.Fprintf(l.w, "INFO: %s", msg)
	// if s, ok := l.w.(syncer); ok { // type assertion
	l.s.Sync()
	// s := l.w.(syncer) // will panic is l.w does not implement syncer
}

type Logger struct {
	w io.Writer
	s syncer
	// w WriteSyncer // Too restrictive
}
