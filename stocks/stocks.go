package main

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
)

func main() {
	related, err := relatedStocks("CYBR")
	if err != nil {
		fmt.Println("ERROR:", err)
		return
	}
	for sym, count := range related {
		if sym == "CYBR" {
			continue
		}
		fmt.Println(sym, "=>", count)
	}
}

func parseResponse(r io.Reader) (map[string]int, error) {
	var reply struct {
		Messages []struct {
			Symbols []struct {
				Name string `json:"symbol"`
			}
		}
	}

	if err := json.NewDecoder(r).Decode(&reply); err != nil {
		return nil, err
	}

	counts := make(map[string]int)
	for _, msg := range reply.Messages {
		for _, sym := range msg.Symbols {
			counts[sym.Name]++
			// if c, ok := counts[sym.Name]; !ok {}  // missing
			// m := {"172": "AWS", "345": "GCP"}
		}
	}

	return counts, nil
}

// relatedStocks return a map of symbol->count of stocks mentioned.
func relatedStocks(symbol string) (map[string]int, error) {
	// Debug: work with file (API rate limit)
	file, err := os.Open("cybr.json")
	if err != nil {
		return nil, err
	}
	defer file.Close()

	return parseResponse(file)
}

// JSON Command Line Tools
// jq: query & indent JSON
// python -m json.tool: indent json
