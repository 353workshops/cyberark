package main

import (
	"fmt"
	"sort"
)

func main() {
	cart := []string{"apple", "lemon", "banana"}
	fmt.Println(cart)
	fmt.Println("cart[0]:", cart[0])
	fmt.Println("len:", len(cart), "cap:", cap(cart))

	// indices
	for i := range cart {
		fmt.Println(i)
	}

	// indices + values
	for i, v := range cart {
		fmt.Println(i, v)
	}

	for _, v := range cart {
		fmt.Println(v)
	}

	// for i := 0; i < len(cart); i++ {
	for range cart {
		fmt.Println("hi")
	}

	cart = append(cart, "bread")
	fmt.Println(cart)
	fmt.Println("len:", len(cart), "cap:", cap(cart))
	cart = append(cart, "milk")
	fmt.Println("len:", len(cart), "cap:", cap(cart))

	//var arr [4]int // arr is an array

	var s []int
	for i := 0; i < 10_000; i++ {
		s = appendInt(s, i)
	}
	fmt.Println(s[:10])

	// fruit := cart[:3] // BUG(?): Will change cart as well
	fruit := cart[:3:3] // full slice notation
	fruit = append(fruit, "pear")
	fmt.Println("fruit:", fruit)
	fmt.Println("cart:", cart)

	out := concat([]string{"A", "B", "C"}, []string{"D", "E"})
	fmt.Println("concat ->", out) // [A B C D E]

	values := []float64{3, 1, 2} // 2 <nil>
	fmt.Println(median(values))
	values = []float64{3, 1, 2, 4}
	fmt.Println(values)
	fmt.Println(median(values)) // 2.5 <nil>
	fmt.Println(values)

}

// naming conventions:
// functions: camelCase
// acronyms in upper case letter: URL
// variables: short names, get longer by distance
func median(values []float64) (float64, error) {
	if len(values) == 0 {
		return 0, fmt.Errorf("median of empty slice")
	}

	// Copy so we won't mutate input values
	vals := make([]float64, len(values))
	copy(vals, values)

	sort.Float64s(vals)
	i := len(vals) / 2
	if len(vals)%2 == 1 {
		return vals[i], nil
	}

	mid := (vals[i-1] + vals[i]) / 2
	return mid, nil
}

// Write a concat function that will concatenate two []string
// concat([]string{"A", "B", "C"}, []string{"D", "E"})
//
//	-> []string{"A", "B", "C", "D", "E"}
//
// Try without a "for" loop
func concat(s1 []string, s2 []string) []string {
	// TODO: if cap(s1) - len(s1) >= len(s2) ???
	out := make([]string, len(s1)+len(s2))
	copy(out, s1)
	copy(out[len(s1):], s2)
	return out
}

// Simulate built-in "append"
func appendInt(s []int, val int) []int {
	i := len(s)
	if len(s) < cap(s) { // There's enough space in the underlying array
		s = s[:len(s)+1]
	} else { // Need to re-allocated & copy
		size := 2 * (len(s) + 1)
		fmt.Println(cap(s), "->", size)
		s2 := make([]int, size) // +1 for 0
		copy(s2, s)
		s = s2[:len(s)+1]
	}

	// v := []int{99:0} // Don't write code like that
	s[i] = val
	return s
}

/*
s
*/
