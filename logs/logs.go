package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"os"
	"time"
)

func main() {
	lr := LogRecord{
		Time:   time.Now().UTC(),
		Level:  "INFO",
		Messge: "Server starting",
		Attrs: map[string]string{
			"env":  "qa",
			"host": "qa-9",
		},
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	if err := send(ctx, lr); err != nil {
		fmt.Println("ERROR:", err)
		return
	}

	var records []LogRecord
	for i := 0; i < 5; i++ {
		rec := lr
		rec.Time = lr.Time.Add(time.Duration(i*100) * time.Millisecond)
		records = append(records, rec)
	}

	ctx, cancel = context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	if err := sendMany(ctx, records); err != nil {
		fmt.Println("ERROR (many):", err)
	}
}

const url = "http://localhost:8080/logs"

func sendMany(ctx context.Context, records []LogRecord) error {
	r, w, err := os.Pipe()
	if err != nil {
		return err
	}
	defer w.Close() // notify producer we're done, avoid goroutine leak

	go func() { // producer
		defer w.Close()
		enc := json.NewEncoder(w)
		for _, lr := range records {
			if err := enc.Encode(lr); err != nil {
				slog.Error("can't encode", "record", lr, "error", err)
				// TODO: Return error with named return variable or sendErr variable
				return
			}
			time.Sleep(100 * time.Millisecond) // simulate work delay
		}
	}()

	req, err := http.NewRequestWithContext(ctx, http.MethodPost, url, r)
	if err != nil {
		return err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf(resp.Status)
	}

	io.Copy(os.Stdout, resp.Body)

	return nil
}

func send(ctx context.Context, lr LogRecord) error {
	// LogRecord -> io.Reader (JSON)
	// Option 1: marshal to []byte, use bytes.NewReader
	// Option: 2: use bytes.Buffer
	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(lr); err != nil {
		return err
	}
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, url, &buf)
	if err != nil {
		return err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf(resp.Status)
	}

	data, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	slog.Info("log send", "reply", string(data))

	return nil
}

type LogRecord struct {
	Time   time.Time
	Level  string
	Messge string
	Attrs  map[string]string
}
