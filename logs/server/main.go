package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"os"
	"time"
)

type Server struct {
	logger *slog.Logger
}

func (s *Server) healthHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "OK")
}

type LogRecord struct {
	Time   time.Time
	Level  string
	Messge string
	Attrs  map[string]string
}

func (s *Server) logsHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "bad method", http.StatusMethodNotAllowed)
		return
	}

	start := time.Now()
	dec := json.NewDecoder(r.Body)
	count := 0
	for {
		var lr LogRecord
		err := dec.Decode(&lr)
		if errors.Is(err, io.EOF) {
			break
		}
		if err != nil {
			s.logger.Error("can't read", "error", err)
			http.Error(w, "bad request", http.StatusBadRequest)
			return
		}
		count++
		s.logger.Info("log", "record", lr)
	}

	reply := map[string]any{
		"count":    count,
		"duration": time.Since(start),
	}
	w.Header().Set("content-type", "application/json")
	if err := json.NewEncoder(w).Encode(reply); err != nil {
		s.logger.Error("can't encode", "error", err)
	}
}

func main() {
	addr := os.Getenv("ADDR")
	if addr == "" {
		addr = ":8080"
	}

	s := Server{slog.Default()}

	mux := http.NewServeMux()
	mux.HandleFunc("/health", s.healthHandler)
	mux.HandleFunc("/logs", s.logsHandler)

	srv := http.Server{
		Addr:    addr,
		Handler: mux,
	}

	s.logger.Info("server starting", "address", addr)
	if err := srv.ListenAndServe(); err != nil {
		fmt.Fprintf(os.Stderr, "error: %s\n", err)
		os.Exit(1)
	}
}
