package main

import (
	"fmt"
)

func main() {
	i := Item{1, 2} // must be in order of declaration
	fmt.Println(i)

	i = Item{Y: 1} //, X: 2} // can be in any order

	// Aside: Printing structs
	fmt.Printf(" v: %v\n", i)
	fmt.Printf("+v: %+v\n", i)
	fmt.Printf("#v: %#v\n", i) // Use %#v for debugging/logging
	a, b := 1, "1"
	fmt.Printf(" v: a=%v, b=%v\n", a, b)
	fmt.Printf("#v: a=%#v, b=%#v\n", a, b)

	fmt.Println(NewItem(1, 2))
	fmt.Println(NewItem(1, 2000))

	i.Move(200, 300) // Works even though Move gets a pointer receiver
	fmt.Println("i (move):", i)

	p1 := Player{
		Name: "Parzival",
		Item: Item{100, 200},
	}
	fmt.Printf("%#v\n", p1)
	fmt.Println("p1.X:", p1.X)
	// fmt.Println("p1.Item.X:", p1.Item.X)
	p1.Move(400, 500)
	fmt.Printf("%#v (move)\n", p1)

	// i3 := Int(3)
	ms := []Mover{
		&p1, // Must use pointers since Move users pointer receiver
		&i,
		//		&i3,
	}
	moveAll(ms, 0, 0)
	for i, m := range ms {
		fmt.Printf("moveAll: %d - %#v\n", i, m)
	}

	key := Jade
	fmt.Printf("key: %v\n", key)

	p1.Found(Jade)
	p1.Found(Jade)
	fmt.Println(p1.Keys)          // [jade]
	fmt.Println(p1.Found(Key(9))) // error
}

func (p *Player) Found(k Key) error {
	if k <= 0 || k >= maxKey {
		return fmt.Errorf("unknown key: %s", k)
	}

	// if slices.Contains(p.Keys, k)
	if p.hasKey(k) {
		return nil
	}

	p.Keys = append(p.Keys, k)
	return nil
}

func (p *Player) hasKey(k Key) bool {
	for _, k2 := range p.Keys {
		if k == k2 {
			return true
		}
	}

	return false
}

/* Exercise:
- Add a field "Keys" which is a slice of Key to Player
- Add a method "Found(k Key) error" to player
	- Should add Key only once
	- Should err if key is not a known key
*/

/* What fmt.Print* does
func print(v any) {
	if f, ok := v.(fmt.Stringer); ok { // type assertion
		_print(f.String())
	}

	switch v.(type) { // type switch
	case int:
		...
	}
}
*/

// go install golang.org/x/tools/cmd/stringer@latest
// export PATH="$(go env GOPATH)/bin:${PATH}"

// String implements fmt.Stringer
func (k Key) String() string {
	switch k {
	case Jade:
		return "jade"
	case Crystal:
		return "crystal"
	case Copper:
		return "copper"
	}

	return fmt.Sprintf("<Key %d>", k)
}

type Key byte

const (
	Crystal Key = iota + 1 // 1 << iota // bitmask
	Jade
	Copper
	maxKey
)

/*
func (i Int) Move(x, y int) {
	fmt.Println("Not moving")
}

type Int int
*/

func moveAll(ms []Mover, x, y int) {
	for _, m := range ms {
		m.Move(x, y)
	}
}

// Rules:
// - Interfaces define what we want, not what we provide
// - Interfaces tend to be small (stdlib avg < 2)
// - Accept interfaces, return types
type Mover interface {
	Move(int, int)
}

type Player struct {
	Name string
	Keys []Key
	//	X    float32 // will "shadow" Player.Item.X
	Item // Player embeds item (this is not inheritance)
}

// i is called "the receiver"
// When you mutate (state) use pointer receiver
// Rule of thumb: Don't mix pointer and value receivers for the same type
func (i *Item) Move(x, y int) {
	i.X = x
	i.Y = y
}

// func NewItem(x, y int) Item {
// func NewItem(x, y int) *Item {
// func NewItem(x, y int) (*Item, error) {
func NewItem(x, y int) (Item, error) {
	if x < 0 || x > maxX || y < 0 || y > maxY {
		return Item{}, fmt.Errorf("NewItem(%d, %d) out of range %d/%d", x, y, maxX, maxY)
	}

	i := Item{X: x, Y: y}
	return i, nil
}

const (
	maxX = 600
	maxY = 400
)

// An item in the game
type Item struct {
	X int
	Y int
}

/* Thought experiment
type Sortable interface {
	Less(i, j int) bool
	Swap(i, j int)
	Len() int
}

func sort(s Sortable) {
	// TODO
}
*/
