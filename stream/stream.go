package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"strings"
)

func main() {
	total := 0
	r := strings.NewReader(data)
	dec := json.NewDecoder(r)
loop:
	for {
		var p Payment
		err := dec.Decode(&p)
		switch {
		case errors.Is(err, io.EOF):
			break loop
		//	goto end
		case err != nil:
			fmt.Println("ERROR:", err)
			return
		}
		//	fmt.Println(dec.InputOffset())
		total += p.Amount
	}
	// end:
	fmt.Println("total:", total)

}

type Payment struct {
	Name   string
	Amount int // cents
}

var data = `
{"name": "bugs", "amount": 123}
{"name": "daffy", "amount": 137}
`
