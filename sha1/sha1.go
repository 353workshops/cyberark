package main

import (
	"compress/gzip"
	"crypto/sha1"
	"fmt"
	"io"
	"os"
	"strings"
)

func main() {
	// GoLand: sha1/http.log.gz
	fmt.Println(sha1Sum("http.log.gz"))
	fmt.Println(sha1Sum("sha1.go"))

	//path := "c:\to\new\report.csv"
	path := `c:\to\new\report.csv` // raw string
	fmt.Println(path)
}

// Exercise: Only decompress if file name ends with .gz
// - See strings.HasSuffix
// cat http.log.gz| gunzip | sha1sum
func sha1Sum(fileName string) (string, error) {
	// cat http.log.gz
	// Idiom: Acquire resource, check for error, defer release
	file, err := os.Open(fileName)
	if err != nil {
		return "", err
	}
	defer file.Close()

	var r io.Reader = file

	if strings.HasSuffix(fileName, ".gz") {
		// | gunzip
		// r, err := gzip.NewReader(file) // BUG: New r only in "if" scope
		r, err = gzip.NewReader(file)
		if err != nil {
			return "", err
		}
		//fmt.Printf("gzip: %p\n", r)
	}

	// | sha1sum
	w := sha1.New()
	if _, err := io.Copy(w, r); err != nil {
		return "", err
	}

	sig := w.Sum(nil)
	return fmt.Sprintf("%x", sig), nil
}

/* Go
type Reader interface {
	Read(p []byte) (int, error)
}

Python
type Reader interface {
	Read(n int) ([]byte, error)
}
*/
