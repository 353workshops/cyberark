package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"time"
)

func main() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	fmt.Println(userInfo(ctx, "cyberark"))
}

// userInfo return user name and nuber of public repos from github
func userInfo(ctx context.Context, login string) (string, int, error) {
	// Exercise: Your code
	// Hint:
	url, err := url.JoinPath("https://api.github.com/users", login)
	if err != nil {
		return "", 0, err
	}

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		return "", 0, err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return "", 0, fmt.Errorf("%q: %w", url, err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return "", 0, fmt.Errorf("%q: %s", url, resp.Status)
	}

	// fmt.Println("ctype:", resp.Header.Get("Content-Type"))
	// io.Copy(os.Stdout, resp.Body)
	var reply struct {
		Name     string
		NumRepos int `json:"public_repos"`
	}
	r := io.LimitReader(resp.Body, 1<<20) // 1MB
	if err := json.NewDecoder(r).Decode(&reply); err != nil {
		return "", 0, fmt.Errorf("%q: can't decode - %w", url, err)
	}

	return reply.Name, reply.NumRepos, nil
}

// curl https://api.github.com/users/cyberark
