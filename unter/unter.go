package unter

import (
	"fmt"

	"github.com/cyberark/unter/backoffice"
	/*
		unter "github.com/cyberark/unter"
		// rename to avoid collision
		unter2 "github.com/cyberark2/unter"
	*/)

// Package A -> C v1.1
// Package B -> C v1.2

// Ride contains a ride information.
type Ride struct {
	ID       string
	Distance float64 // km
	Shared   bool
}

func (r Ride) Fare() int {
	return backoffice.RideFare(r.Distance, r.Shared)
}

// Validate validates that the ride is OK.
func (r Ride) Validate() error {
	if r.ID == "" {
		return fmt.Errorf("empty ride ID")
	}

	if r.Distance <= 0 {
		return fmt.Errorf("%#v - bad distance", r)
	}

	return nil
}
