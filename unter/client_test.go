package unter

import (
	"fmt"
	"net"
	"net/http"
	"os"
	"os/exec"
	"path"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func buildServer(t *testing.T) string {
	exe := path.Join(t.TempDir(), "httpd")
	cmd := exec.Command("go", "build", "-o", exe, "./cmd/httpd/")
	err := cmd.Run()
	require.NoError(t, err)
	return exe
}

func freePort(t *testing.T) int {
	conn, err := net.Listen("tcp", "")
	require.NoError(t, err)
	conn.Close()
	return conn.Addr().(*net.TCPAddr).Port
}

func waitForServer(t *testing.T, addr string, timeout time.Duration) {
	start := time.Now()
	var err error
	for time.Since(start) < timeout {
		var conn net.Conn
		conn, err = net.Dial("tcp", addr)
		if err == nil {
			conn.Close()
			return
		}
		time.Sleep(10 * time.Millisecond)
	}

	t.Fatalf("server at %s not ready after %s - %s ", addr, timeout, err)
}

func runServer(t *testing.T) int {
	exe := buildServer(t)
	port := freePort(t)
	t.Logf("%q starts on %d", exe, port)

	cmd := exec.Command(exe)
	//	cmd.Stderr = os.Stderr

	cmd.Env = append(os.Environ(), fmt.Sprintf("ADDR=:%d", port))
	err := cmd.Start()
	require.NoError(t, err)
	t.Cleanup(func() {
		if err := cmd.Process.Kill(); err != nil {
			t.Logf("can't kill %d - %s", cmd.Process.Pid, err)
		}
	})

	waitForServer(t, fmt.Sprintf("localhost:%d", port), time.Second)

	return port
}

func TestClient_Health(t *testing.T) {
	port := runServer(t)
	url := fmt.Sprintf("http://localhost:%d", port)
	c := NewClient(url)
	err := c.Health()
	require.NoError(t, err)
}

func TestClient_HealthError(t *testing.T) {
	c := NewClient("https://example.com")
	c.c.Transport = mockTripper{t}
	err := c.Health()
	require.Error(t, err)
}

func (m mockTripper) RoundTrip(r *http.Request) (*http.Response, error) {
	m.t.Logf("RoundTrip for %s", r.URL.Path)
	return nil, fmt.Errorf("can't connect")
}

type mockTripper struct {
	t *testing.T
}
