package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"path"
)

func healthHandler(w http.ResponseWriter, r *http.Request) {
	// TODO
	fmt.Fprintln(w, "OK")
}

var (
	opts struct {
		ShowVersion bool
	}
	Version = "<unknown>"
)

func main() {
	flag.BoolVar(&opts.ShowVersion, "version", false, "show version & exit")
	flag.Parse()

	if opts.ShowVersion {
		fmt.Printf("%s version %s\n", path.Base(os.Args[0]), Version)
		os.Exit(0)
	}

	var mux http.ServeMux
	// http.HandleFunc("/health", healthHandler)
	mux.HandleFunc("/health", healthHandler)

	addr := os.Getenv("ADDR")
	if addr == "" {
		addr = ":8080"
	}

	log.Printf("INFO: server starting on %s", addr)
	srv := http.Server{
		Handler: &mux,
		///...
	}
	// if err := http.ListenAndServe(addr, nil); err != nil {
	if err := srv.ListenAndServe(); err != nil {
		log.Printf("ERROR: can't run - %s", err)
		os.Exit(1)
	}
}

/* Building
go build ./cmd/httpd
CGO_ENABLED=0 go build ./cmd/httpd
GOOS=darwin GOARCH=arm64 go build ./cmd/httpd
go build -ldflags='-X main.Version=1.2.3' ./cmd/httpd

See also GoReleaser.
*/
