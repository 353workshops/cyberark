package main

import (
	_ "embed"
	"flag"
	"fmt"
	"log"
	"os"
	"path"

	"github.com/cyberark/unter"
)

// Configuration: defaults < config file < environment < command line
// defaults: code
// config file: YAML, TOML (not in stdlib)
// environment: os.Getenv
// command line: flag
// External packages:
// - cobra (command line), viper (configuraiton)
// - ardanlabs/conf

var (
	//go:embed sql/get.sql
	getSQL string

	opts struct {
		LogSQL bool
	}
)

func main() {
	flag.Usage = func() {
		name := path.Base(os.Args[0])
		fmt.Fprintf(os.Stderr, "usage: %s get ID\nOptions:\n", name)
		flag.PrintDefaults()
	}
	flag.BoolVar(&opts.LogSQL, "log-sql", false, "log SQL queries")
	flag.Parse()
	if flag.NArg() != 2 {
		fmt.Fprintf(os.Stderr, "error: wrong number of arguments\n")
		os.Exit(1)
	}

	id := flag.Arg(1)
	ride, err := getRide(id)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error: %q - no such ride (%s)\n", id, err)
		os.Exit(1)
	}

	fmt.Printf("%+v\n", ride)
}

func getRide(id string) (unter.Ride, error) {
	if opts.LogSQL {
		log.Printf("DEBUG: get %s, sql: \n%s", id, getSQL)
	}

	return unter.Ride{}, fmt.Errorf("not implemented")
}
