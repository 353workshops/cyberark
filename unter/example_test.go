package unter_test

import (
	"fmt"

	"github.com/cyberark/unter"
)

func ExampleRide_Validate() {
	r := unter.Ride{
		ID:       "123",
		Distance: 3.2,
	}
	fmt.Println(r.Validate())

	r.Distance = -3
	fmt.Println(r.Validate())

	// Output:
	// <nil>
	// unter.Ride{ID:"123", Distance:-3, Shared:false} - bad distance
}
