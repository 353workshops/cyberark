package backoffice

import "math"

const (
	baseFare = 150
)

// RideFare return the ride frae in US cents.
func RideFare(distance float64, shared bool) int {
	fare := float64(baseFare) + (math.Ceil(distance) * 100)
	if shared {
		fare *= 0.9
	}

	return int(fare)
}
