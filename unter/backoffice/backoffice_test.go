package backoffice

import (
	"testing"

	"github.com/stretchr/testify/require"
)

var feeCases = []struct {
	name     string
	distance float64
	shared   bool
	fare     int
}{
	{"regular", 3, false, 450},
	{"shared", 3, true, 405},
	{"round up", 2.1, false, 450},
}

func TestRideFare(t *testing.T) {
	for _, tc := range feeCases {
		t.Run(tc.name, func(t *testing.T) {
			fare := RideFare(tc.distance, tc.shared)
			require.Equal(t, tc.fare, fare)
		})
	}

}
