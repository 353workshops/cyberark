module github.com/cyberark/unter

go 1.21

// go mod init github.com/cyberark/unter

require (
	github.com/stretchr/testify v1.8.4
	gopkg.in/yaml.v3 v3.0.1
)

// replace gopkg.in/yaml.v3 => ./patches/yaml

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
)
