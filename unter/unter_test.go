package unter

import (
	"log"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gopkg.in/yaml.v3"
)

// Package level fixture
func TestMain(m *testing.M) {
	log.Printf("package level setup")
	out := m.Run()
	log.Printf("package level teardown")
	os.Exit(out)
}

// function level fixtures:
// setup - call a function
// teardown - defer or t.Cleanup

// yaml.NewDecoder(r /* io.Reader */).Decode(&cases)

/*
var validateCases = []struct { // anonymous struct
	Name     string
	ID       string
	Distance float64
	Error    bool
}{
	{"ok", "123", 1.2, false},
	{"empty id", "", 1.2, true},
	{"negative distance", "123", -2.3, true},
}
*/

func FuzzRide_Validate(f *testing.F) {
	f.Add("0", 0.0) // will run a regular test
	f.Fuzz(func(t *testing.T, id string, distance float64) {
		r := Ride{ID: id, Distance: distance}
		err := r.Validate()
		if id == "" || distance <= 0 {
			require.Errorf(t, err, "no error for %q %f", id, distance)
			return
		}

		if err != nil {
			require.NoErrorf(t, err, "unexpected error: %#v - %s", r)
		}
	})
}

// go test -v -fuzz .
// failing cases go to testdata/fuzz (use f.Add to add them & delete)

type validateCase struct {
	Name     string
	ID       string
	Distance float64
	Error    bool
}

func loadValidateCases(t *testing.T) []validateCase {
	file, err := os.Open("testdata/validate_cases.yml")
	require.NoError(t, err)
	defer file.Close() // If you return the file, use t.Cleanup
	var cases []validateCase
	err = yaml.NewDecoder(file).Decode(&cases)
	require.NoError(t, err, "yaml")
	return cases
}

// Exercise: Read tests cases from testdata/validate_cases.yml
// (instead of in-memory validateCases slice)
func TestRide_ValiateTable(t *testing.T) {
	for _, tc := range loadValidateCases(t) {
		t.Run(tc.Name, func(t *testing.T) {
			r := Ride{ID: tc.ID, Distance: tc.Distance}
			err := r.Validate()
			require.Falsef(t, err == nil && tc.Error, "%#v - unexpected error", r)
			require.Falsef(t, err != nil && !tc.Error, "%#v - no error", r)
			/*
					if err != nil && !tc.Error {
						t.Fatalf("%#v: unexpected error - %s", r, err)
					}

				if err == nil && tc.Error {
					t.Fatalf("%#v: no error", r)
				}
			*/
		})
	}
}

func TestRide_Valiate(t *testing.T) {
	r := Ride{ID: "234", Distance: 1.2}
	err := r.Validate()
	require.NoErrorf(t, err, "%#v: unexpected error", r)
	/*
		if err != nil {
			t.Fatalf("%#v: unexpected error - %s", r, err)
		}
	*/
}

// go test -v
// go help testflag
// go test -run Example
