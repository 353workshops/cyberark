### Go Paint

Implement a paining program. It should support

- Circle with location (x, y), color and radius
- Rectangle with location, color, width and height

Each shape should implement a `Draw(d Device)` method. Where `Device` is:

```go
type Device interface {
	Set(int, int, color.Color)
}
```

Implement an `ImageCanvas` struct which holds a slice of drawable items and has `Draw` methods that writes a PNG to an `io.Writer`. (using `image/png`)

```go
func (ic *ImageCanvas) Draw(w io.Writer) error {
    // ...
}
```

Example test:
```go
var (
	Red   = color.RGBA{0xFF, 0, 0, 0xFF}
	Green = color.RGBA{0, 0xFF, 0, 0xFF}
	Blue  = color.RGBA{0, 0, 0xFF, 0xFF}
)

// ...

ic, err := NewImageCanvas(200, 200)
if err != nil {
	log.Fatal(err)
}

ic.Add(&Circle{100, 100, 80, Green})
ic.Add(&Circle{60, 60, 10, Blue})
ic.Add(&Circle{140, 60, 10, Blue})
ic.Add(&Rectangle{100, 130, 80, 10, Red})
f, err := os.Create("face.png")
if err != nil {
	log.Fatal(err)
}
defer f.Close()
if err := ic.Draw(f); err != nil {
	log.Fatal(err)
}
```

This should produce
![](face.png)
