### Exercise

Some function in Go has a `Must` version that is intended to be used in `var` declaration.
For example, regexp has `func Compile(expr string) (*Regexp, error)` and also `func MustCompile(str string) *Regexp` that is like `Compile` but will panic on error.

Write a generic `Must` function that will get a function with a single argument and return a value and error,
and will return a function which accepts an argument and returns value or panics on error.

For example (`must_test.go`):

```go
package must

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
)

type Ring struct{}

func NewRing(size int) (*Ring, error) {
	if size < 1 {
		return nil, fmt.Errorf("size (%d) <= 0", size)
	}

	return &Ring{}, nil
}

var MustRing = Must(NewRing)

func TestMust(t *testing.T) {
	r := MustRing(3)
	require.NotNil(t, r)
	require.Panics(t, func() { MustRing(-2) })
}
```

Extension:
- Support functions that except more than one parameter.
    - Maybe `go generate` up to `N` parameters?
- Support variadic function (e.g. `func NewVector(n ...int) (*Vector, error)`)
