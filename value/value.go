package main

import (
	"encoding/json"
	"fmt"
)

func main() {
	v := Value{15.4, Celsius}
	// fmt.Println(v)
	data, err := json.Marshal(v) // data is []byte
	if err != nil {
		fmt.Println("ERROR:", err)
		return
	}
	fmt.Println(string(data))

	var v2 Value
	if err := json.Unmarshal(data, &v2); err != nil {
		fmt.Println("ERROR (unmarshal):", err)
	}
	fmt.Printf("%#v\n", v2)

}

func (v Value) MarshalJSON() ([]byte, error) {
	// Step 1: Convert to type known to encoding/json
	s := fmt.Sprintf("%f%s", v.Amount, v.Unit)
	// Step 2: Use json.Marshal
	return json.Marshal(s)
	// Step 3: There is no step 3
}

func (v *Value) UnmarshalJSON(data []byte) error {
	// Exercise: You do it!
	// Hint: fmt.SScanf("12.3c", "%f%s", &f, &s)
	s := string(data[1 : len(data)-1]) // trim ""
	var a float64
	var u Unit

	if _, err := fmt.Sscanf(s, "%f%s", &a, &u); err != nil {
		return err
	}

	if err := u.Validate(); err != nil {
		return err
	}

	v.Amount = a
	v.Unit = u
	return nil
}

// Value if a measurement value
type Value struct {
	Amount float64 `json:"amount,omitempty"` // field tag
	Unit   Unit    `json:"unit,omitempty"`
}

type Unit string

const (
	Celsius    Unit = "c"
	Fahrenheit Unit = "f"
	Kelvin     Unit = "k"
)

func (u Unit) Validate() error {
	switch u {
	case Celsius, Fahrenheit, Kelvin:
		return nil
	}

	return fmt.Errorf("unknown unit: %q", u)
}

// We want to marshal to JSON as "12.5c"

/*
# Go <-> JSON types
nil <-> null
bool <-> bool
string <-> string
float64, float32, int, int8 ..., uint, uint 8 ... <-> number
[]any, []T <-> array
map[string]any, struct <-> object

Extra types
time.Time -> string (RFC3339)
[]byte -> string (base64)

# encoding/json API
JSON -> []byte -> Go: json.Unmarshal
Go -> []byte -> JSON: json.Marshal
JSON -> io.Reader -> Go: json.NewDecoder
Go -> io.Writer -> JSON: json.NewEncoder

Only exported fields are handled.
Unknown fields (both sides) are ignored.

*/
