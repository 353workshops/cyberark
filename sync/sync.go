package main

import (
	"fmt"
	"sync"
)

func main() {
	// floats are a hoax
	// fmt.Printf("%.30f\n", 0.1+0.1+0.1)

	p := Payment{
		From:   "Wile. E. Coyote",
		To:     "ACME",
		Amount: 123.45,
	}
	p.Pay()
	p.Pay()
}

func (p *Payment) Pay() {
	p.once.Do(p.pay)
}
func (p *Payment) pay() {
	fmt.Printf("%s -> [$%.2f] -> %s\n", p.From, p.Amount, p.To)
}

type Payment struct {
	/*
		mu sync.Mutex
		done bool
	*/
	once sync.Once

	From   string
	To     string
	Amount float32 // in USD
}
