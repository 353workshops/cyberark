# Go Workshop @ CyberArk

Miki Tebeka
📬 [miki@353solutions.com.com](mailto:miki@353solutions.com), 𝕏 [@tebeka](https://twitter.com/tebeka), 👨 [mikitebeka](https://www.linkedin.com/in/mikitebeka/), ✒️[blog](https://www.ardanlabs.com/blog/)

#### Shameless Plugs

- [LinkedIn Learning Classes](https://www.linkedin.com/learning/instructors/miki-tebeka)
- [Books](https://pragprog.com/search/?q=miki+tebeka)

[Syllabus](_extra/syllabus.pdf)  
[Final exercise](_extra/dld.md)


---

## Day 1: Data semantics & polymorphism with interfaces

### Agenda

- Mechanical sympathy and the effect on Go data semantics
    - Stack vs heap allocations
    - Computer latency
- Slices and how they work
    - Underlying arrays
    - Slicing
    - How “append” works
- Value & pointer receivers
- Struct embedding
- Using interfaces for polymorphism
    - Small interfaces
    - Interfaces in the standard library
    - Keep interfaces small with type assertions
- Using generics to reduce code size and create type-safe code

### Code

- [deep.go](deep/deep.go) - Generics
- [logger.go](logger/logger.go) - Keeping interfaces small with type assertions
- [sha1.go](sha1/sha1.go) - Combining `io.Reader`s
- [game.go](game/game.go) - Structs, methods & interfaces
- [slices.go](slices/slices.go) - Working with slices
- [vm.go](vm/vm.go) - Points & values

### Exercises

- Read and understand the [sort package examples](https://pkg.go.dev/sort#pkg-overview)
- [Drawing](_extra/draw.md)
- [Must](_extra/must.md)

### Links

- [stringer](https://pkg.go.dev/golang.org/x/tools/cmd/stringer)
    - `go install golang.org/x/tools/cmd/stringer@latest`
    - `stringer -type Key` (in `game`)
        - In your bashrc: `export PATH="$(go env GOPATH)/bin:${PATH}"`
- [Build your own gopher](https://gopherize.me/)
- Go in Israel
    - [GopherCon Israel](https://www.gophercon.org.il/)
    - [Go Israel Meetup](https://www.meetup.com/Go-Israel/)
- [Why is a Goroutine’s stack infinite ?](https://dave.cheney.net/2013/06/02/why-is-a-goroutines-stack-infinite)
- [Go proverbs](https://go-proverbs.github.io/)
- [Computer Latency at Human Scale](https://twitter.com/jordancurve/status/1108475342468120576/photo/1)
- [Garbage Collection in Go](https://www.ardanlabs.com/blog/2018/12/garbage-collection-in-go-part1-semantics.html)
- [Slice Tricks Cheatsheet](https://ueokande.github.io/go-slice-tricks/)
- [Go Slices: usage and internals](https://go.dev/blog/slices-intro)
- [Methods, interfaces & embedded types in Go](https://www.ardanlabs.com/blog/2014/05/methods-interfaces-and-embedded-types.html)
- [Methods & Interfaces](https://go.dev/tour/methods/1) in the Go tour
- [sort examples](https://pkg.go.dev/sort/#pkg-examples) - Read and try to understand
- [Generics can make your Go code slower](https://planetscale.com/blog/generics-can-make-your-go-code-slower)
- [When to use generics](https://go.dev/blog/when-generics)
- [Generics tutorial](https://go.dev/doc/tutorial/generics)

### Data & Other

- [Call Stack](_extra/stack.png)
- [CPU Cache](_extra/cpu-cache.png)
- [http.log.gz](_extra/http.log.gz)

---

## Day 2: Concurrency & Performance

### Agenda
- How Go’s runtime handles concurrency
- Goroutines
- Using channels and their semantics
- Using context.Context for timeouts & cancellations
- Working with “sync” and “sync/atomic” packages
- Performance optimization
    - CPU optimization: Measuring code speed and using the profiler
    - Memory optimization: Measuring memory consumption and using the profiler
    - A look at the execution tracer and external tools
    - General rules and guidelines

### Code


- [go_chan.go](go_chan/go_chan.go) - Goroutines & channels
- [stack.go](stack/stack.go) - Generic data structures
- [auth.go](auth/auth.go) - Implement your own `error`
- [fan_in.go](fan_in/fan_in.go) - "Fan in" channels
- [taxi.go](taxi/taxi.go) - Convert sequential algorithm to parallel
- [rtb.go](rtb/rtb.go) - Using `select` and `context.Context` for timeout and cancellations
- [sync.go](sync/sync.go) - A look at the `sync` package
- [counter.go](counter/counter.go) - The race detector and `sync/atomic`
- [tokenizer](tokenizer) - Performance optimization

### Exercise

In [taxi.go](taxi/taxi.go), create:
```go
func CheckDirectory(ctx context.Context, rootDir string, n int) error {
}
```

It should check the validity of files as we did in class, but limit the runtime by `ctx` and use only `n` goroutines at a time.

### Links

- [How to Lie with Statistics](https://en.wikipedia.org/wiki/How_to_Lie_with_Statistics)
- [LRU cache](https://github.com/hashicorp/golang-lru) from HashiCorp
- [Why does Go not support methods with type parameters?](https://go.dev/doc/faq#generic_brackets)
- [Tar joke](https://xkcd.com/1168/)
- Inspecting executables
    - `go tool nm`
    - [go list, your Swiss army knife](https://dave.cheney.net/2014/09/14/go-list-your-swiss-army-knife)
- [Using Go Analysis to Write a Custom Linter](https://arslan.io/2019/06/13/using-go-analysis-to-write-a-custom-linter/)
- Concurrency
    - [An Introduction to go tool trace](https://about.sourcegraph.com/blog/go/an-introduction-to-go-tool-trace-rhys-hiltner)
    - [The race detector](https://go.dev/doc/articles/race_detector)
    - [Context](https://blog.golang.org/context) on the Go blog
    - [Data Race Patterns in Go](https://eng.uber.com/data-race-patterns-in-go/)
    - [Go Concurrency Patterns: Pipelines and cancellation](https://go.dev/blog/pipelines)
    - [Go Concurrency Patterns: Context](https://go.dev/blog/context)
    - [Curious Channels](https://dave.cheney.net/2013/04/30/curious-channels)
    - [The Behavior of Channels](https://www.ardanlabs.com/blog/2017/10/the-behavior-of-channels.html)
    - [Channel Semantics](https://www.353solutions.com/channel-semantics)
    - [Why are there nil channels in Go?](https://medium.com/justforfunc/why-are-there-nil-channels-in-go-9877cc0b2308)
    - [Amdahl's Law](https://en.wikipedia.org/wiki/Amdahl%27s_law) - Limits of concurrency
    - [Computer Latency at Human Scale](https://twitter.com/jordancurve/status/1108475342468120576/photo/1)
    - [Concurrency is not Parallelism](https://www.youtube.com/watch?v=cN_DpYBzKso) by Rob Pike
    - [Scheduling in Go](https://www.ardanlabs.com/blog/2018/08/scheduling-in-go-part2.html) by Bill Kennedy
    - [errgroup](https://pkg.go.dev/golang.org/x/sync/errgroup#example-Group-JustErrors) - `sync.WaitGroup` with errors
- Performance
    - [Rules of Optimization Club](https://wiki.c2.com/?RulesOfOptimizationClub)
    - [benchstat](https://pkg.go.dev/golang.org/x/perf/cmd/benchstat)
    - [Benchmarking Checklist](https://www.brendangregg.com/blog/2018-06-30/benchmarking-checklist.html)
    - [Rob Pike's 5 Rules of Programming](https://users.ece.utexas.edu/~adnan/pike.html)
    - [Computer Latency at Human Scale](https://twitter.com/jordancurve/status/1108475342468120576)
    - [A Guide to the Go Garbage Collector](https://go.dev/doc/gc-guide)
    - [Garbage Collection In Go](https://www.ardanlabs.com/blog/2018/12/garbage-collection-in-go-part1-semantics.html)
    - [Getting Friendly with CPU Caches](https://www.ardanlabs.com/blog/2023/07/getting-friendly-with-cpu-caches.html)
    - [Language Mechanics](https://www.ardanlabs.com/blog/2017/05/language-mechanics-on-stacks-and-pointers.html)
    - [Tracing Programs with Go](https://www.youtube.com/watch?v=mjixFKO-IdM)
    - [High Performance Go Workshop](https://dave.cheney.net/high-performance-go-workshop/gophercon-2019.html)
    - [High Performance Networking in Chrome](https://aosabook.org/en/posa/high-performance-networking-in-chrome.html)
    - [graphviz](https://graphviz.org/)
    - [Profile-guided optimization](https://go.dev/doc/pgo)

### Data & Other

- [taxi.tar](https://storage.googleapis.com/353solutions/c/data/taxi.tar)
- [Miki's optimization guidelines](_extra/optimize.md)

---

## Day 3: Project Engineering

### Agenda

- Structuring your code
- Documenting your code
    - Writing test-able documentation
- Testing
    - The “testify” package
    - Table testing
    - Fuzzing
    - Running services
    - Mocking
    - Package-level fixtures
- Dependency management
    - Go modules and how they work
    - Vendoring and the “replace” directive
- Configuration patterns
    - viper
- Logging & metrics
    - slog
- Building executables
    - Static executables
    - Embedding assets and setting version information
    - Docker builds
- Writing Secure Go Code
    - Patching an insecure app
    - OWASP Top 10 in Go
    - Security tools

### Code

- [unter](unter) project - Engineering
- [journal](journal) project - Security


### Exercises

- Add an option in `unter/cmd/httpd` to get the port from the command line
- Find more security holes in journal
    - Email me the code annotated with `// SECURITY: ` comments

### Links

- [Perfectly Reproducible, Verified Go Toolchains](https://go.dev/blog/rebuild)
- [Hyrum's Law](https://www.hyrumslaw.com/)
- [starship](https://starship.rs/) - Cross shell prompt
- `//go:` comments
    - [Customizing Go Binaries with Build Tags](https://www.digitalocean.com/community/tutorials/customizing-go-binaries-with-build-tags)
    - [Go's Hidden Pragmas](https://dave.cheney.net/2018/01/08/gos-hidden-pragmas)
    - [Go compiler directives](https://pkg.go.dev/cmd/compile#hdr-Compiler_Directives)
    - [Generating code](https://go.dev/blog/generate)
    - The [embed](https://pkg.go.dev/embed/) package
- [Writing Deployable Code (part one)](https://medium.com/@rantav/writing-deployable-code-part-one-13ec6dc90adb)
- [Athens](https://github.com/gomods/athens) - Go modules proxy server
- [GoReleaser](https://goreleaser.com/) - Publish Go executables
- [Conway's Law](https://martinfowler.com/bliki/ConwaysLaw.html)
- [The Twelve-Factor App](https://12factor.net)
- Configuration
    - [conf](https://pkg.go.dev/github.com/ardanlabs/conf/v3)
    - [viper](https://github.com/spf13/viper) & [cobra](https://github.com/spf13/cobra)
    - [conf](https://pkg.go.dev/github.com/ardanlabs/conf/v3)
- Logging 
    - Built-in [slog](https://pkg.go.dev/log/slog)
    - [uber/zap](https://pkg.go.dev/go.uber.org/zap)
    - [logrus](https://github.com/sirupsen/logrus)
    - The experimental [slog](https://pkg.go.dev/golang.org/x/exp/slog)
- Testing
    - [testing](https://pkg.go.dev/testing)
    - [testify](https://pkg.go.dev/github.com/stretchr/testify)
    - [Test containers](https://golang.testcontainers.org/features/creating_container/)
    - [Tutorial: Getting started with fuzzing](https://go.dev/doc/tutorial/fuzz)
    - [testing/quick](https://pkg.go.dev/testing/quick)
    - [How SQLite is Tested](https://www.sqlite.org/testing.html)
- Metrics
    - Built-in [expvar](https://pkg.go.dev/expvar/)
    - [Open Telemetry](https://opentelemetry.io/)
    - [Prometheus](https://pkg.go.dev/github.com/prometheus/client_golang/prometheus)
- [Go Code Review Comments](https://github.com/golang/go/wiki/CodeReviewComments)
- [Tutorial: Getting started with multi-module workspaces](https://go.dev/doc/tutorial/workspaces)
- [Organizing Go Modules](https://go.dev/doc/modules/layout#server-project)
- [Example Project Structure](https://github.com/ardanlabs/service)
- [How to Write Go Code](https://go.dev/doc/code.html)
- Documentation
    - [Godoc: documenting Go code](https://go.dev/blog/godoc)
    - [Go Doc Comments](https://go.dev/doc/comment)
    - [Testable examples in Go](https://go.dev/blog/examples)
    - [Go documentation tricks](https://godoc.org/github.com/fluhus/godoc-tricks)
    - [gob/doc.go](https://github.com/golang/go/blob/master/src/encoding/gob/doc.go) of the `gob` package. Generates [this documentation](https://pkg.go.dev/encoding/gob/)
    - `go install golang.org/x/pkgsite/cmd/pkgsite@latest` (require go 1.18+)
    - `pkgsite -http=:8080` (open browser on http://localhost:8080/${module name})
- [Out Software Dependency Problem](https://research.swtch.com/deps) - Good read on dependencies by Russ Cox
- [Customizing Binaries with Build Tags](https://www.digitalocean.com/community/tutorials/customizing-go-binaries-with-build-tags)
- Linters (static analysis)
    - [staticcheck](https://staticcheck.io/)
    - [golangci-lint](https://golangci-lint.run/)
    - [gosec](https://github.com/securego/gosec) - Security oriented
    - [vulncheck](https://pkg.go.dev/golang.org/x/vuln/vulncheck) - Check for CVEs
    - [golang.org/x/tools/go/analysis](https://pkg.go.dev/golang.org/x/tools/go/analysis) - Helpers to write analysis tools (see [example](https://arslan.io/2019/06/13/using-go-analysis-to-write-a-custom-linter/))
- Authorization
    - [open policy](https://github.com/open-policy-agent/opa)
    - [casbin](https://casbin.org/)
- Security Books
    - [Security with Go](https://www.packtpub.com/product/security-with-go/9781788627917)
    - [Black Hat Go](https://nostarch.com/blackhatgo) book
- Limiting Size
    - [MaxBytesHandler](https://pkg.go.dev/net/http#MaxBytesHandler)
    - [MaxBytesReader](https://pkg.go.dev/net/http#MaxBytesReader)
    - [io.LimitReader](https://pkg.go.dev/io#LimitReader)
- Validation
    - [cue](https://cuelang.org/) - Language for data validation
    - [validator](https://github.com/go-playground/validator)
- Serialization Vulnerabilities
    - [XML Billion Laughs](https://en.wikipedia.org/wiki/Billion_laughs_attack) attack
    - [Java Parse Float](https://www.exploringbinary.com/java-hangs-when-converting-2-2250738585072012e-308/)
- [Understanding HTML templates in Go](https://blog.lu4p.xyz/posts/golang-template-turbo/)
- [Resilient net/http servers](https://ieftimov.com/post/make-resilient-golang-net-http-servers-using-timeouts-deadlines-context-cancellation/)
- [Our Software Depenedcy Problem](https://research.swtch.com/deps)
- [Go's CVE List](https://www.cvedetails.com/vulnerability-list/vendor_id-14185/Golang.html)
- Static tools
    - [govulncheck](https://pkg.go.dev/golang.org/x/vuln/cmd/govulncheck)
    - [gosec](https://github.com/securego/gosec)
    - [gitleaks](https://github.com/gitleaks/gitleaks)
- [OWASP Top 10](https://owasp.org/www-project-top-ten/)
- [OWASP Go Secure Coding Practices Guide](https://owasp.org/www-project-go-secure-coding-practices-guide/)
- [The Security Mindset](https://www.schneier.com/blog/archives/2008/03/the_security_mi_1.html) by Bruce Schneier

### Data & Other

- [Call Stack](_extra/stack.png)
- [CPU Cache](_extra/cpu-cache.png)
- [http.log.gz](_extra/http.log.gz)

---

## Day 2: Concurrency & Performance

### Agenda
- How Go’s runtime handles concurrency
- Goroutines
- Using channels and their semantics
- Using context.Context for timeouts & cancellations
- Working with “sync” and “sync/atomic” packages
- Performance optimization
    - CPU optimization: Measuring code speed and using the profiler
    - Memory optimization: Measuring memory consumption and using the profiler
    - A look at the execution tracer and external tools
    - General rules and guidelines

### Code


- [go_chan.go](go_chan/go_chan.go) - Goroutines & channels
- [stack.go](stack/stack.go) - Generic data structures
- [auth.go](auth/auth.go) - Implement your own `error`
- [fan_in.go](fan_in/fan_in.go) - "Fan in" channels
- [taxi.go](taxi/taxi.go) - Convert sequential algorithm to parallel
- [rtb.go](rtb/rtb.go) - Using `select` and `context.Context` for timeout and cancellations
- [sync.go](sync/sync.go) - A look at the `sync` package
- [counter.go](counter/counter.go) - The race detector and `sync/atomic`
- [tokenizer](tokenizer) - Performance optimization

### Exercise

In [taxi.go](taxi/taxi.go), create:
```go
func CheckDirectory(ctx context.Context, rootDir string, n int) error {
}
```

It should check the validity of files as we did in class, but limit the runtime by `ctx` and use only `n` goroutines at a time.

### Links

- [How to Lie with Statistics](https://en.wikipedia.org/wiki/How_to_Lie_with_Statistics)
- [LRU cache](https://github.com/hashicorp/golang-lru) from HashiCorp
- [Why does Go not support methods with type parameters?](https://go.dev/doc/faq#generic_brackets)
- [Tar joke](https://xkcd.com/1168/)
- Inspecting executables
    - `go tool nm`
    - [go list, your Swiss army knife](https://dave.cheney.net/2014/09/14/go-list-your-swiss-army-knife)
- [Using Go Analysis to Write a Custom Linter](https://arslan.io/2019/06/13/using-go-analysis-to-write-a-custom-linter/)
- Concurrency
    - [An Introduction to go tool trace](https://about.sourcegraph.com/blog/go/an-introduction-to-go-tool-trace-rhys-hiltner)
    - [The race detector](https://go.dev/doc/articles/race_detector)
    - [Context](https://blog.golang.org/context) on the Go blog
    - [Data Race Patterns in Go](https://eng.uber.com/data-race-patterns-in-go/)
    - [Go Concurrency Patterns: Pipelines and cancellation](https://go.dev/blog/pipelines)
    - [Go Concurrency Patterns: Context](https://go.dev/blog/context)
    - [Curious Channels](https://dave.cheney.net/2013/04/30/curious-channels)
    - [The Behavior of Channels](https://www.ardanlabs.com/blog/2017/10/the-behavior-of-channels.html)
    - [Channel Semantics](https://www.353solutions.com/channel-semantics)
    - [Why are there nil channels in Go?](https://medium.com/justforfunc/why-are-there-nil-channels-in-go-9877cc0b2308)
    - [Amdahl's Law](https://en.wikipedia.org/wiki/Amdahl%27s_law) - Limits of concurrency
    - [Computer Latency at Human Scale](https://twitter.com/jordancurve/status/1108475342468120576/photo/1)
    - [Concurrency is not Parallelism](https://www.youtube.com/watch?v=cN_DpYBzKso) by Rob Pike
    - [Scheduling in Go](https://www.ardanlabs.com/blog/2018/08/scheduling-in-go-part2.html) by Bill Kennedy
    - [errgroup](https://pkg.go.dev/golang.org/x/sync/errgroup#example-Group-JustErrors) - `sync.WaitGroup` with errors
- Performance
    - [Rules of Optimization Club](https://wiki.c2.com/?RulesOfOptimizationClub)
    - [benchstat](https://pkg.go.dev/golang.org/x/perf/cmd/benchstat)
    - [Benchmarking Checklist](https://www.brendangregg.com/blog/2018-06-30/benchmarking-checklist.html)
    - [Rob Pike's 5 Rules of Programming](https://users.ece.utexas.edu/~adnan/pike.html)
    - [Computer Latency at Human Scale](https://twitter.com/jordancurve/status/1108475342468120576)
    - [A Guide to the Go Garbage Collector](https://go.dev/doc/gc-guide)
    - [Garbage Collection In Go](https://www.ardanlabs.com/blog/2018/12/garbage-collection-in-go-part1-semantics.html)
    - [Getting Friendly with CPU Caches](https://www.ardanlabs.com/blog/2023/07/getting-friendly-with-cpu-caches.html)
    - [Language Mechanics](https://www.ardanlabs.com/blog/2017/05/language-mechanics-on-stacks-and-pointers.html)
    - [Tracing Programs with Go](https://www.youtube.com/watch?v=mjixFKO-IdM)
    - [High Performance Go Workshop](https://dave.cheney.net/high-performance-go-workshop/gophercon-2019.html)
    - [High Performance Networking in Chrome](https://aosabook.org/en/posa/high-performance-networking-in-chrome.html)
    - [graphviz](https://graphviz.org/)
    - [Profile-guided optimization](https://go.dev/doc/pgo)

### Data & Other

- [taxi.tar](https://storage.googleapis.com/353solutions/c/data/taxi.tar)
- [Miki's optimization guidelines](_extra/optimize.md)

---

## Day 3: Project Engineering

### Agenda

- Structuring your code
- Documenting your code
    - Writing test-able documentation
- Testing
    - The “testify” package
    - Table testing
    - Fuzzing
    - Running services
    - Mocking
    - Package-level fixtures
- Dependency management
    - Go modules and how they work
    - Vendoring and the “replace” directive
- Configuration patterns
    - viper
- Logging & metrics
    - slog
- Building executables
    - Static executables
    - Embedding assets and setting version information
    - Docker builds
- Writing Secure Go Code
    - Patching an insecure app
    - OWASP Top 10 in Go
    - Security tools

### Code

- [unter](unter) project - Engineering
- [journal](journal) project - Security


### Exercises

- Add an option in `unter/cmd/httpd` to get the port from the command line
- Find more security holes in journal
    - Email me the code annotated with `// SECURITY: ` comments
    - Fix them

### Links

- [memguard](https://pkg.go.dev/github.com/awnumar/memguard) - Secure memory
- [Hyrum's Law](https://www.hyrumslaw.com/)
- [starship](https://starship.rs/) - Cross shell prompt
- [Go's Hidden Pragmas](https://dave.cheney.net/2018/01/08/gos-hidden-pragmas)
- [Writing Deployable Code (part one)](https://medium.com/@rantav/writing-deployable-code-part-one-13ec6dc90adb)
- [Athens](https://github.com/gomods/athens) - Go modules proxy server
- [GoReleaser](https://goreleaser.com/) - Publish Go executables
- [Conway's Law](https://martinfowler.com/bliki/ConwaysLaw.html)
- [The Twelve-Factor App](https://12factor.net)
- Configuration
    - [conf](https://pkg.go.dev/github.com/ardanlabs/conf/v3)
    - [viper](https://github.com/spf13/viper) & [cobra](https://github.com/spf13/cobra)
    - [conf](https://pkg.go.dev/github.com/ardanlabs/conf/v3)
- Logging 
    - Built-in [slog](https://pkg.go.dev/log/slog)
    - [uber/zap](https://pkg.go.dev/go.uber.org/zap)
    - [logrus](https://github.com/sirupsen/logrus)
    - The experimental [slog](https://pkg.go.dev/golang.org/x/exp/slog)
- Testing
    - [testing](https://pkg.go.dev/testing)
    - [testify](https://pkg.go.dev/github.com/stretchr/testify)
    - [Test containers](https://golang.testcontainers.org/features/creating_container/)
    - [Tutorial: Getting started with fuzzing](https://go.dev/doc/tutorial/fuzz)
    - [testing/quick](https://pkg.go.dev/testing/quick)
    - [How SQLite is Tested](https://www.sqlite.org/testing.html)
- Metrics
    - Built-in [expvar](https://pkg.go.dev/expvar/)
    - [Open Telemetry](https://opentelemetry.io/)
    - [Prometheus](https://pkg.go.dev/github.com/prometheus/client_golang/prometheus)
- [Go Code Review Comments](https://github.com/golang/go/wiki/CodeReviewComments)
- [Tutorial: Getting started with multi-module workspaces](https://go.dev/doc/tutorial/workspaces)
- [Organizing Go Modules](https://go.dev/doc/modules/layout#server-project)
- [Example Project Structure](https://github.com/ardanlabs/service)
- [How to Write Go Code](https://go.dev/doc/code.html)
- Documentation
    - [Godoc: documenting Go code](https://go.dev/blog/godoc)
    - [Go Doc Comments](https://go.dev/doc/comment)
    - [Testable examples in Go](https://go.dev/blog/examples)
    - [Go documentation tricks](https://godoc.org/github.com/fluhus/godoc-tricks)
    - [gob/doc.go](https://github.com/golang/go/blob/master/src/encoding/gob/doc.go) of the `gob` package. Generates [this documentation](https://pkg.go.dev/encoding/gob/)
    - `go install golang.org/x/pkgsite/cmd/pkgsite@latest` (require go 1.18+)
    - `pkgsite -http=:8080` (open browser on http://localhost:8080/${module name})
- [Out Software Dependency Problem](https://research.swtch.com/deps) - Good read on dependencies by Russ Cox
- [Customizing Binaries with Build Tags](https://www.digitalocean.com/community/tutorials/customizing-go-binaries-with-build-tags)
- Linters (static analysis)
    - [staticcheck](https://staticcheck.io/)
    - [golangci-lint](https://golangci-lint.run/)
    - [gosec](https://github.com/securego/gosec) - Security oriented
    - [vulncheck](https://pkg.go.dev/golang.org/x/vuln/vulncheck) - Check for CVEs
    - [golang.org/x/tools/go/analysis](https://pkg.go.dev/golang.org/x/tools/go/analysis) - Helpers to write analysis tools (see [example](https://arslan.io/2019/06/13/using-go-analysis-to-write-a-custom-linter/))
- The new [embed](https://pkg.go.dev/embed/) package
- Authorization
    - [open policy](https://github.com/open-policy-agent/opa)
    - [casbin](https://casbin.org/)
- Security Books
    - [Security with Go](https://www.packtpub.com/product/security-with-go/9781788627917)
    - [Black Hat Go](https://nostarch.com/blackhatgo) book
- Limiting Size
    - [MaxBytesHandler](https://pkg.go.dev/net/http#MaxBytesHandler)
    - [MaxBytesReader](https://pkg.go.dev/net/http#MaxBytesReader)
    - [io.LimitReader](https://pkg.go.dev/io#LimitReader)
- Validation
    - [cue](https://cuelang.org/) - Language for data validation
    - [validator](https://github.com/go-playground/validator)
- Serialization Vulnerabilities
    - [XML Billion Laughs](https://en.wikipedia.org/wiki/Billion_laughs_attack) attack
    - [Java Parse Float](https://www.exploringbinary.com/java-hangs-when-converting-2-2250738585072012e-308/)
- [Understanding HTML templates in Go](https://blog.lu4p.xyz/posts/golang-template-turbo/)
- [Resilient net/http servers](https://ieftimov.com/post/make-resilient-golang-net-http-servers-using-timeouts-deadlines-context-cancellation/)
- [Our Software Depenedcy Problem](https://research.swtch.com/deps)
- [Go's CVE List](https://www.cvedetails.com/vulnerability-list/vendor_id-14185/Golang.html)
- Static tools
    - [govulncheck](https://pkg.go.dev/golang.org/x/vuln/cmd/govulncheck)
    - [gosec](https://github.com/securego/gosec)
    - [gitleaks](https://github.com/gitleaks/gitleaks)
- [OWASP Top 10](https://owasp.org/www-project-top-ten/)
- [OWASP Go Secure Coding Practices Guide](https://owasp.org/www-project-go-secure-coding-practices-guide/)
- [The Security Mindset](https://www.schneier.com/blog/archives/2008/03/the_security_mi_1.html) by Bruce Schneier

### Data & Other
- [Writing Secure Go Code](_extra/secure-code.pdf)

---

## Day 4: Web Services

### Agenda

- Working with JSON
    - Custom marshaling
    - Zero vs missing values
- HTTP clients
    - Timeouts and cancellations
    - Request body
    - Steaming requests
- HTTP Servers
    - Writing middleware
    - Handling incoming data
    - Passing request information downstream
    - Testing handlers & servers
    - Profiling and debugging running servers

### Code

- [value.go](value/value.go) - Custom JSON marshaling
- [missing.go](missing/missing.go) - Handling missing values (missing vs zero)
- [stocks.go](stocks/stocks.go) - Handling large nested JSON
- [stream.go](stream/stream.go) - Steaming JSON
- [github.go](github/github.go) - HTTP client (GET)
- [logs.go](logs/logs.go) - HTTP client POST + streaming
- [weather.go](weather/weather.go) - HTTP Server (handlers, middleware, auth, profile)

### Links

- [goleak](https://github.com/uber-go/goleak) - Goroutine leak detector
- [x/rate](https://pkg.go.dev/golang.org/x/time/rate) - Rate limiting
- [So you want to expose Go on the Internet](https://blog.cloudflare.com/exposing-go-on-the-internet/)
- SQL
    - [sqlc](https://docs.sqlc.dev) - Generate Go code from SQL queries
    - [sqlx](https://github.com/jmoiron/sqlx) - Extends database/sql to work with structs
    - [gorm](https://gorm.io/index.html) - ORM
- [Falsehoods programmers believe about time zones](https://www.zainrizvi.io/blog/falsehoods-programmers-believe-about-time-zones/)
- [Falsehoods programmers believe about time](https://infiniteundo.com/post/25326999628/falsehoods-programmers-believe-about-time)
- [json.RawMessage](https://pkg.go.dev/encoding/json#RawMessage) - Partial parse of JSON
- [http.cat](https://http.cat/) - HTTP status codes in cats
- [cleanhttp](https://github.com/hashicorp/go-cleanhttp) - Clean (no shared transport) HTTP client
- [httpbin.org](https://httpbin.org/#/) - Test your HTTP clients
- [encoding/json](https://golang.org/pkg/encoding/json/) - Use this
- [Custom marshaling](https://golang.org/pkg/encoding/json/#example__customMarshalJSON)
- [Using anonymous nested structs](https://pythonwise.blogspot.com/2020/01/nested-structs-for-http-calls.html)
- [mapstructure](https://github.com/mitchellh/mapstructure) - Decode `map[string]any` to a struct
- [Chunked HTTP in Go](https://www.alexedwards.net/blog/how-to-use-the-http-responsecontroller-type)
- [jsonlines](https://jsonlines.org/examples/)
- HTTP Servers
    - [net/http](https://golang.org/pkg/net/http/) - Built-in HTTP client & server
    - [gorilla/mux](http://www.gorillatoolkit.org/pkg/mux) - More flexible mux
    - [gin](https://github.com/gin-gonic/gin)
    - [chi](https://github.com/go-chi/chi) - Web framework
    - [fasthttp](https://godoc.org/github.com/valyala/fasthttp) - Faster HTTP server, use *only* if you really need it
- [Making & Using HTTP Middleware](https://www.alexedwards.net/blog/making-and-using-middleware)
- [delve](https://github.com/go-delve/delve) - The Go debugger
    - [Using with Docker](https://github.com/go-delve/delve/blob/master/Documentation/faq.md#docker)
    - [Debug a Go Application running on Kubernetes cluster](https://youtu.be/YXu2box7z9k?si=yoYQvGg6odsjk9VE) by Alexei Ledenev
- [Ultimate Go Debugging](https://www.ardanlabs.com/training/ultimate-go/debugging/)
- Debug Running Server:
    - Terminal 1

            go build -gcflags='-N -l'
            ./wather

    - Terminal 2

            sudo dlv attach <pid>
            break weather.go:105
            c

    - Terminal 3

            curl -u joe:baz00ka -d@record.json http://localhost:8080`
- Profile Running Server
    - Run server
    - Terminal 1
            
            hey -z 10s http://localhost:8080/health

    - Terminal 2

            go tool pprof -http :8888 http://localhost:8080/debug/pprof/profile?seconds=5


### Data & More

- `curl https://api.stocktwits.com/api/2/streams/symbol/CYBR.json`
- [log.json](_extra/log.json)
- [record.json](_extra/record.json)
    - `curl -u joe:baz00ka -d@record.json http://localhost:8080`
- `golang.org/x/exp/slog`
