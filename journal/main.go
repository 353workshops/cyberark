package main

import (
	_ "embed"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
)

var (
	//go:embed html/last.html
	lastHTML string
)

type Server struct {
	db *DB
}

func (s *Server) lastHandler(w http.ResponseWriter, r *http.Request) {
	lastText := "No entries"

	e, err := s.db.Last()
	if err == nil {
		time := e.Time.Format("2006-01-02T15:04")
		lastText = fmt.Sprintf("[%s] %s by %s", time, e.Content, e.Login)
	}
	// SECURITY: XSS
	fmt.Fprintf(w, lastHTML, lastText)
}

func (s *Server) newHandler(w http.ResponseWriter, r *http.Request) {
	// SECURITY: Rate limit, authentication & authorization
	defer r.Body.Close()
	var e Entry

	if err := json.NewDecoder(r.Body).Decode(&e); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		// SECURITY: No return after http.Error
	}

	// SECURITY: Missing data validation

	e.Time = time.Now()
	err := s.db.Add(e)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	resp := map[string]interface{}{
		"error": nil,
		"time":  e.Time,
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(resp)
}

func (s *Server) Health() error {
	return s.db.Health()
}

func (s *Server) healthHandler(w http.ResponseWriter, r *http.Request) {
	if err := s.Health(); err != nil {
		// SECURITY: Error unchanged to client
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	fmt.Fprintf(w, "OK\n")
}

// Run once before to create the database:
// go run ./cmd/dbutil journal.db
// To add and entry:
// curl -d@testdata/add-1.json http://localhost:8080/api/journal
func main() {
	var err error
	// SECURITY: Dynamic configuration
	dsn := os.Getenv("JOURNAL_DSN")
	if dsn == "" {
		// SECURITY: Hard coded password
		dsn = "journal.db?user=joe&password=baz00ka"
	}
	db, err := NewDB(dsn)
	if err != nil {
		log.Fatal(err)
	}

	s := Server{db}

	r := mux.NewRouter()
	r.HandleFunc("/last", s.lastHandler).Methods("GET")
	r.HandleFunc("/api/journal", s.newHandler).Methods("POST")
	r.HandleFunc("/api/health", s.healthHandler).Methods("GET")

	http.Handle("/", r)

	addr := os.Getenv("JOURNAL_ADDR")
	if addr == "" {
		addr = ":8080"
	}
	log.Printf("server starting on %s", addr)
	// SECURITY: No TLS, No timeouts, using default server
	if err := http.ListenAndServe(addr, nil); err != nil {
		log.Fatal(err)
	}
}
