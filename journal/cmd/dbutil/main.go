package main

import (
	"database/sql"
	_ "embed"
	"flag"
	"fmt"
	"os"

	_ "modernc.org/sqlite"
)

var (
	//go:embed schema.sql
	schemaSQL string
)

func main() {
	flag.Parse()
	if flag.NArg() != 1 {
		fmt.Fprintln(os.Stderr, "error: wrong number of arguments")
		os.Exit(1)
	}

	dbFile := flag.Arg(0)
	db, err := sql.Open("sqlite", dbFile)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error: %q - %s\n", dbFile, err)
		os.Exit(1)
	}
	defer db.Close()

	db.Exec(schemaSQL)
}
